# Old-Time Music Memory
![banjo player](https://s-media-cache-ak0.pinimg.com/originals/e9/78/db/e978dbdfa4138c82c03c3592ac7d2046.jpg)

Worried your memory is going? Want to help preserve the history of traditional Appalachian music by getting to know its pioneering musicians? Kill two birds with one stone and exercise your mental muscles while paying homage to a few heroes of old-time music.

## Setup
Clone this repository and run `yarn install` (or `npm install`), then `yarn start` (or `npm start`) in the root
directory to start up the development server. Once it’s running, you can view
the app at `localhost:3000`.

## Architecture
This is a small demo application built with with React and Redux, which leverages core concepts of functional programming. The architectural design is highly inspired by the Elm architecture. I used Redux to manage state and redux-loop to manage side effects like DOM manipulation, and `setTimeout`-ing. One benefit of redux-loop is that it allows you to _describe_ asynchronous side effects within the reducer, but these side effects are deferred for later and never directly called in the reducer. This ensures that the reducer remains a pure -- and thus easily testable -- function.

Additionally, I used only stateless, functional React components within the app also because of how easy they are to test and implement. State and actions are passed directly into the components where necessary. In keeping with the principle of _'functional core, imperative shell'_, there is one outer file - `src/index.js` - that acts as the imperative shell and contains side-effects like calling `ReactDOM`'s `render` function and `store.dispatch`. Everything within that shell is a pure function.

## Tests
1. Component snapshot tests to test the views.
2. Reducer test with traditional Jest expectations to verify every action returns the proper state and side effects.
3. Unit tests on util functions where necessary.

## Photo credits
* Elizabeth Cotten: Photo from the Southern Historical Collection, The Wilson Library, University of North Carolina at Chapel Hill

*  Hazel and Alice: Photo by John Cohen

* Tommy Jarrell: Photo from the film Sprout Wings and Fly by Les Blank, Cece Conway, Alice Gerrard, and Maureen Gosling

* Joe Thompson: Photo by Lissa Gotwals

* Clyde Davenport: Photo from Jeff Titon

* Marcus Martin: Photo from Jerry and Joan Johnson

* Samantha Bumgarner: Photo from the Southern Folklife Collection

* Cousin Emmy: Photo from Berea College, Southern Appalachian Archives, John Lair Collection
