import React from 'react';
import { render } from 'react-dom';
import { combineReducers, install as installReduxLoop } from 'redux-loop';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore } from 'redux';
import Game from './components/game';
import gameReducer from './reducer';
import timerReducer from './components/timer/reducer';
import { selectCard, shuffleCards } from './actions';

const root = document.querySelector('#root');

const reducer = combineReducers({
  game: gameReducer,
  timer: timerReducer
});

const store = createStore(reducer, composeWithDevTools(installReduxLoop()));

const renderApp = () =>
  render(
    <Game
      timer={store.getState().timer}
      state={store.getState().game}
      selectCard={id => store.dispatch(selectCard(id))}
    />,
    root
  );

renderApp();
store.subscribe(renderApp);
store.dispatch(shuffleCards());
