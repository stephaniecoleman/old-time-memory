import { Cmd, loop } from 'redux-loop';
import { cacheIntervalId, incrementTimer } from './actions';

export const initialState = {
  intervalId: null,
  secondsElapsed: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CACHE_INTERVAL_ID': {
      return { ...state, intervalId: action.id };
    }

    case 'INCREMENT_TIMER': {
      return { ...state, secondsElapsed: state.secondsElapsed + 1 };
    }

    case 'START_TIMER': {
      return loop(
        state,
        Cmd.run(startClock, {
          successActionCreator: cacheIntervalId,
          args: [Cmd.dispatch],
        })
      );
    }

    case 'STOP_TIMER': {
      return loop(state, Cmd.run(stopClock, { args: [state.intervalId] }));
    }

    default: {
      return state;
    }
  }
};

/**
 *
 * SIDE EFFECTS
 *
 */

export const startClock = dispatch =>
  setInterval(() => dispatch(incrementTimer()), 1000);

export const stopClock = intervalId => clearInterval(intervalId);

export default reducer;
