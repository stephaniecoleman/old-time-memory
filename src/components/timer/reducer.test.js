import { Cmd } from 'redux-loop';
  import {
    cacheIntervalId,
    incrementTimer,
    startTimer,
    stopTimer,
  } from './actions';
import reducer, { initialState, startClock, stopClock } from './reducer';

describe('reducer', () => {
  describe('CACHE_INTERVAL_ID', () => {
    it('returns the correct state', () => {
      const newState = reducer(initialState, cacheIntervalId(123));
      expect(newState).toMatchObject({
        intervalId: 123,
      });
    });
  });

  describe('INCREMENT_TIMER', () => {
    it('returns the correct state', () => {
      const newState = reducer(initialState, incrementTimer());
      expect(newState).toMatchObject({
        secondsElapsed: 1,
      });
    });
  });

  describe('START_TIMER', () => {
    it('returns the proper state and side effects', () => {
      const [newState, { type, func, args, successActionCreator }] = reducer(
        initialState,
        startTimer()
      );
      expect(type).toBe('RUN');
      expect(func).toBe(startClock);
      expect(args).toEqual([Cmd.dispatch]);
      expect(successActionCreator).toBe(cacheIntervalId);
      expect(newState).toMatchObject(initialState);
    });
  });

  describe('STOP_TIMER', () => {
    it('returns the proper state and side effects', () => {
      const state = { ...initialState, intervalId: 123 };
      const [newState, { type, func, args }] = reducer(state, stopTimer());
      expect(type).toBe('RUN');
      expect(func).toBe(stopClock);
      expect(args).toEqual([123]);
      expect(newState).toMatchObject(state);
    });
  });
});

jest.useFakeTimers();

describe('side effects', () => {
  describe('startClock', () => {
    it('calls a function every 1 second', () => {
      const callback = jest.fn();
      startClock(callback);
      expect(setInterval).toHaveBeenCalled();

      jest.runTimersToTime(1000);
      expect(callback).toHaveBeenCalledTimes(1);

      jest.runTimersToTime(1000);
      expect(callback).toHaveBeenCalledTimes(2);
    });
  });

  describe('stopClock', () => {
    it('clears the timer', () => {
      stopClock(123);
      expect(clearInterval).toHaveBeenCalledWith(123);
    });
  });
});
