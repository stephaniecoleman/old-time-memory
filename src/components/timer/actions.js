export const incrementTimer = () => ({
  type: 'INCREMENT_TIMER',
});

export const cacheIntervalId = id => ({
  type: 'CACHE_INTERVAL_ID',
  id,
});

export const stopTimer = () => ({
  type: 'STOP_TIMER',
});

export const startTimer = () => ({
  type: 'START_TIMER',
});
