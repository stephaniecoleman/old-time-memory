import React from 'react';
import './styles.css';

const Timer = ({ time }) => (
  <h3 className={'timer'}>
    {time ? formatTime(time) : 'Select a card to begin.'}
  </h3>
);

const formatTime = seconds => {
  const date = new Date(null);
  date.setSeconds(seconds);

  return date
    .toISOString()
    .substr(11, 8)
    .replace(/00:/, '');
};

export default Timer;
