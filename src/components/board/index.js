import React from 'react';
import Card from '../card';
import './styles.css';

const Board = ({ allCards, cardsById, canMakeSelection, selectCard }) => {
  const cards = allCards.map((image, index) => {
    const card = cardsById[index];

    return (
      <Card
        card={card}
        canMakeSelection={canMakeSelection}
        selectCard={selectCard}
        key={card.id}
      />
    );
  });

  return <div className={'board'}>{cards}</div>;
};

export default Board;
