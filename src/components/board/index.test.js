import React from 'react';
import { shallow } from 'enzyme';
import Board from './index';
import { hydratedState, stateWithOneFlipped } from '../../fixtures';

describe('Board', () => {
  describe('with all unmatched cards', () => {
    it('matches snapshot', () => {
      const { allCards, canMakeSelection, cardsById } = hydratedState;
      const component = (
        <Board
          selectCard={() => {}}
          allCards={allCards}
          canMakeSelection={canMakeSelection}
          cardsById={cardsById}
        />
      );

      const wrapper = shallow(component);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('with one flipped card', () => {
    it('matches snapshot', () => {
      const { allCards, canMakeSelection, cardsById } = stateWithOneFlipped;
      const component = (
        <Board
          selectCard={() => {}}
          allCards={allCards}
          canMakeSelection={canMakeSelection}
          cardsById={cardsById}
        />
      );
      const wrapper = shallow(component);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
