import React from 'react';
import renderer from 'react-test-renderer';
import Game from './index';
import { hydratedState } from '../../fixtures';

describe('Game', () => {
  describe('when loading data', () => {
    it('matches snapshot', () => {
      const state = {
        ...hydratedState,
        isLoading: true,
      };
      const component = <Game selectCard={() => {}} state={state} />;
      const tree = renderer.create(component).toJSON();
      expect(tree).toMatchSnapshot();
    });
  });

  describe('when user has won the game', () => {
    it('matches snapshot', () => {
      const state = {
        ...hydratedState,
        winner: true,
      };
      const component = <Game selectCard={() => {}} state={state} />;
      const tree = renderer.create(component).toJSON();
      expect(tree).toMatchSnapshot();
    });
  });

  describe('when there is an error', () => {
    it('matches snapshot', () => {
      const state = {
        ...hydratedState,
        error: 'error!',
      };
      const component = <Game selectCard={() => {}} state={state} />;
      const tree = renderer.create(component).toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
