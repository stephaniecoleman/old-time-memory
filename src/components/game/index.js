import React from 'react';
import Board from '../board';
import Timer from '../timer';
import './styles.css';

const Alert = ({ error, isLoading, winner }) => {
  // Make JS object immutable.
  const text = Object.freeze({
    IS_LOADING: 'Game is loading...',
    WINNER: 'You matched all the cards. Nice job!',
    ERROR: 'Something has gone wrong. Please reload the page.',
  });

  return (
    <div className="alert">
      <h3>{winner && text.WINNER}</h3>
      <h3>{isLoading && text.IS_LOADING}</h3>
      <h3>{error && text.ERROR}</h3>
    </div>
  );
};

const Game = ({ state, selectCard, timer }) => (
  <div className={'game'}>
    <h2>Old Time Music</h2>
    <h1 className={'header'}>Memory</h1>
    {state && (
      <div>
        <Timer time={timer.secondsElapsed} />
        <Alert
          error={state.error}
          isLoading={state.isLoading}
          winner={state.winner}
        />
        {!state.isLoading &&
          !state.winner &&
          !state.error && (
            <Board
              allCards={state.allCards}
              canMakeSelection={state.canMakeSelection}
              cardsById={state.cardsById}
              selectCard={selectCard}
            />
          )}
      </div>
    )}
  </div>
);

export default Game;
