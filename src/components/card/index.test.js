import { shallow } from 'enzyme';
import React from 'react';
import Card from './index';
import { oneCard } from '../../fixtures';

describe('Card', () => {
  describe('when it is unflipped', () => {
    it('matches snapshot', () => {
      const component = (
        <Card selectCard={() => {}} canMakeSelection card={oneCard} />
      );
      const wrapper = shallow(component);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('when it is flipped', () => {
    it('matches snapshot', () => {
      const card = {
        ...oneCard,
        flipped: true,
      };
      const component = (
        <Card selectCard={() => {}} canMakeSelection card={card} />
      );
      const wrapper = shallow(component);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('when it is matched', () => {
    it('matches snapshot', () => {
      const card = {
        ...oneCard,
        flipped: true,
        matched: true,
      };
      const component = (
        <Card selectCard={() => {}} canMakeSelection card={card} />
      );
      const wrapper = shallow(component);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
