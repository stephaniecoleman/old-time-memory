import React from 'react';
import './styles.css';
import defaultImage from '../../assets/images/square.png';

const Card = ({ canMakeSelection, card, selectCard }) => {
  const { image, flipped, id, matched } = card;

  const cardImage = require(`../../assets/images/${image}.png`);

  return (
    <button
      onClick={() => canMakeSelection && selectCard(id)}
      className={['card']
        .concat(matched ? ' matchedCard' : ' unmatchedCard')
        .join('')}
    >
      <img
        className={'image'}
        src={flipped ? cardImage : defaultImage}
        alt={flipped ? `${image} card` : 'face down card'}
      />
    </button>
  );
};

export default Card;
