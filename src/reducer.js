import { Cmd, loop } from 'redux-loop';
import {
  processFlip,
  removeMatchedPair,
  resetIncorrectPair,
  setDeck,
} from './actions';
import initialState from './state';
import { normalizeData, updateFlippedCard } from './utils';
import { startTimer, stopTimer } from './components/timer/actions';

/**
 *
 * REDUCER
 *
 */
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'PROCESS_FLIP': {
      const firstGuess = state.cardsById[state.firstGuessId];
      const selectedCard = action.selectedCard;
      /**
       *
       *  You flip a card...
       *
       *  ...And no card has been previously flipped yet.
       *
       */
      if (state.firstGuessId === null) {
        return {
          ...state,
          cardsById: updateFlippedCard(state, selectedCard),
          firstGuessId: selectedCard.id,
        };

        /**
         *
         *  ...And one card has been previously flipped...
         *
         * ...and they match!
         *
         */
      } else if (selectedCard.image === firstGuess.image) {
        const cardsFlipped = state.cardsFlipped + 2;
        const winner = cardsFlipped === state.allCards.length;

        return loop(
          {
            ...state,
            canMakeSelection: false,
            cardsById: updateFlippedCard(state, selectedCard),
            cardsFlipped,
            firstGuessId: null,
            winner,
          },
          // Wait a moment before flipping correctly matched pair,
          // so user can see that they match.
          Cmd.list([
            winner ? Cmd.action(stopTimer()) : Cmd.none,
            Cmd.run(wait, {
              successActionCreator: removeMatchedPair,
              args: [[selectedCard, firstGuess]],
            }),
          ])
        );
      }

      /**
       *
       * ...and they DO NOT match!
       *
       */
      return loop(
        {
          ...state,
          canMakeSelection: false,
          cardsById: updateFlippedCard(state, selectedCard),
          firstGuessId: null,
        },
        // Wait a moment before flipping incorrectly matched pair,
        // so user can see that they don't match.
        Cmd.run(wait, {
          successActionCreator: resetIncorrectPair,
          args: [[selectedCard, firstGuess]],
        })
      );
    }

    case 'REMOVE_MATCHED_PAIR': {
      const [firstCard, secondCard] = action.matchedPair;

      return {
        ...state,
        canMakeSelection: true,
        cardsById: {
          ...state.cardsById,
          [firstCard.id]: { ...firstCard, matched: true },
          [secondCard.id]: { ...secondCard, matched: true },
        },
      };
    }

    case 'RESET_INCORRECT_PAIR': {
      const [firstCard, secondCard] = action.incorrectPair;

      return {
        ...state,
        canMakeSelection: true,
        cardsById: {
          ...state.cardsById,
          [firstCard.id]: { ...firstCard, flipped: false },
          [secondCard.id]: { ...secondCard, flipped: false },
        },
      };
    }

    case 'SELECT_CARD': {
      const selectedCard = state.cardsById[action.id];

      if (!state.gameStarted) {
        return loop(
          { ...state, gameStarted: true },
          Cmd.list([
            Cmd.action(startTimer()),
            Cmd.action(processFlip(selectedCard)),
          ])
        );
      }

      return selectedCard.flipped
        ? state
        : loop(state, Cmd.action(processFlip(selectedCard)));
    }

    case 'SET_DECK': {
      return {
        ...state,
        allCards: action.deck,
        cardsById: normalizeData(action.deck),
        isLoading: false,
      };
    }

    case 'SHUFFLE_CARDS': {
      return loop(
        state,
        Cmd.run(shuffleCardsEffect, {
          successActionCreator: setDeck,
          args: [state.images],
        })
      );
    }

    default:
      return state;
  }
};

/**
 *
 * SIDE EFFECTS
 *
 */

// Using the Durstenfeld shuffle here. Source:
// stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
export const shuffleCardsEffect = images => {
  // Create a array of pairs by spreading the array of images twice.
  const deck = [...images, ...images];

  // Iterate through each index in the array, starting at the last item.
  // Grab a random index from the indices below the current index.
  // Swap the two values using destructuring assignment.
  for (let i = deck.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [deck[i], deck[j]] = [deck[j], deck[i]];
  }

  return deck;
};

// Wrap setTimeout in a promise to force it to run within a tidy,
// predictable promise chain.
export const wait = (...args) =>
  new Promise(resolve => setTimeout(() => resolve(...args), 1000));

export default reducer;
