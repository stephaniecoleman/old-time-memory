import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// Configuring Enzyme. (http://airbnb.io/enzyme/docs/installation/index.html)
configure({ adapter: new Adapter() });

