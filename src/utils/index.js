// Normalize the card data here so that we don't have to map over the entire
// array of cards to find a particular card object. Instead, we can directly
// grab a card object by its index.
export const normalizeData = cards =>
  cards.reduce(
    (accu, image, index) => ({
      ...accu,
      [index]: {
        id: index,
        image,
        flipped: false,
        matched: false,
      },
    }),
    {}
  );

export const updateFlippedCard = (state, card) => ({
  ...state.cardsById,
  [card.id]: {
    ...card,
    flipped: !card.flipped,
  },
});
