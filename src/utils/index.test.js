import { normalizeData } from './index';

describe('normalizeData', () => {
  it('reduces an array into a normalized object', () => {
    const result = normalizeData(['dog', 'cat']);
    expect(result).toEqual({
      '0': { flipped: false, id: 0, matched: false, image: 'dog' },
      '1': { flipped: false, id: 1, matched: false, image: 'cat' },
    });
  });
});
