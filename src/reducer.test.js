import reducer, {
  shuffleCardsEffect,
  wait,
} from './reducer';
import {
  processFlip,
  removeMatchedPair,
  resetIncorrectPair,
  selectCard,
  setDeck,
  shuffleCards,
} from './actions';
import {
  hydratedState,
  incorrectPair,
  correctPair,
  stateWithOneFlipped,
} from './fixtures';
import initialState from './state';
import { normalizeData } from './utils';

const simpleInitialState = {
  ...initialState,
  images: ['cat', 'dog', 'lion', 'tiger'],
};

describe('reducer', () => {
  describe('PROCESS_FLIP', () => {
    describe('when no cards have been previously flipped', () => {
      it('it returns the correct state and side effects', () => {
        const card = hydratedState.cardsById[0];
        const newState = reducer(hydratedState, processFlip(card));
        expect(newState).toMatchObject({
          cardsById: { 0: { flipped: true } },
          firstGuessId: 0,
        });
      });
    });

    describe('when one card has been previously flipped', () => {
      describe('and it does not match', () => {
        it('returns the correct state and side effects', () => {
          const card = hydratedState.cardsById[1];
          const [newState, { func, successActionCreator }] = reducer(
            stateWithOneFlipped,
            processFlip(card)
          );
          expect(newState).toMatchObject({
            cardsById: { 1: { flipped: true } },
            firstGuessId: null,
          });
          expect(func).toBe(wait);
          expect(successActionCreator).toBe(resetIncorrectPair);
        });
      });

      describe('and it does match', () => {
        it('returns the correct state and side effects', () => {
          const card = hydratedState.cardsById[2];
          const firstGuess =
            stateWithOneFlipped.cardsById[stateWithOneFlipped.firstGuessId];
          const [newState, { cmds: [firstEffect, secondEffect] }] = reducer(
            stateWithOneFlipped,
            processFlip(card)
          );

          expect(newState).toMatchObject({
            cardsById: { 2: { flipped: true } },
            firstGuessId: null,
            winner: false,
            cardsFlipped: 2,
          });

          expect(firstEffect.type).toBe('NONE');
          expect(secondEffect.type).toBe('RUN');
          expect(secondEffect.func).toBe(wait);
          expect(secondEffect.successActionCreator).toBe(removeMatchedPair);
          expect(secondEffect.args).toEqual([[card, firstGuess]]);
        });
      });
    });
  });

  describe('REMOVE_MATCHED_PAIR', () => {
    it('returns the correct state', () => {
      const newState = reducer(hydratedState, removeMatchedPair(correctPair));
      expect(newState).toMatchObject({
        canMakeSelection: true,
        cardsById: { 5: { matched: true }, 7: { matched: true } },
      });
    });
  });

  describe('RESET_INCORRECT_PAIR', () => {
    it('returns the correct state', () => {
      const newState = reducer(
        hydratedState,
        resetIncorrectPair(incorrectPair)
      );
      expect(newState).toMatchObject({
        canMakeSelection: true,
        cardsById: { 5: { matched: false }, 6: { matched: false } },
      });
    });
  });

  describe('SELECT_CARD', () => {
    describe('when the selected card has been previously flipped over', () => {
      it('returns state as is', () => {
        const [newState] = reducer(stateWithOneFlipped, selectCard(0));
        expect(newState).toMatchObject({
          cardsById: { 0: { flipped: true } },
        });
      });
    });
  });
});

describe('SET_DECK', () => {
  it('returns the proper state', () => {
    const deck = ['dog', 'dog', 'cat', 'lion', 'cat', 'lion'];
    const newState = reducer(simpleInitialState, setDeck(deck));
    expect(newState).toMatchObject({
      allCards: deck,
      cardsById: normalizeData(deck),
      isLoading: false,
    });
  });
});

describe('SHUFFLE_CARDS', () => {
  it('returns the proper state and side effects', () => {
    const [newState, { type, func, successActionCreator, args }] = reducer(
      simpleInitialState,
      shuffleCards()
    );
    expect(type).toBe('RUN');
    expect(func).toBe(shuffleCardsEffect);
    expect(successActionCreator).toBe(setDeck);
    expect(args).toEqual([simpleInitialState.images]);
    expect(newState).toEqual(simpleInitialState);
  });
});

/**
 *
 * SIDE EFFECTS
 *
 */
jest.useFakeTimers();

describe('wait', () => {
  it('returns a promise', () => {
    expect.assertions(1);

    wait('hello').then(data => {
      expect(data).toBe('hello');
    });

    jest.runAllTimers();
  });
});
