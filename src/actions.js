export const processFlip = selectedCard => ({
  type: 'PROCESS_FLIP',
  selectedCard
});

export const selectCard = id => ({
  type: 'SELECT_CARD',
  id,
});

export const setDeck = deck => ({
  type: 'SET_DECK',
  deck
});

export const shuffleCards = () => ({
  type: 'SHUFFLE_CARDS'
});

export const resetIncorrectPair = incorrectPair => ({
  type: 'RESET_INCORRECT_PAIR',
  incorrectPair,
});

export const removeMatchedPair = matchedPair => ({
  type: 'REMOVE_MATCHED_PAIR',
  matchedPair,
});
