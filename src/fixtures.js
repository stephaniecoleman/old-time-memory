export const hydratedState = {
  allCards: ['dog', 'cat', 'dog', 'lion', 'lion', 'tiger', 'cat', 'tiger'],
  canMakeSelection: true,
  cardsById: {
    '0': {
      id: 0,
      image: 'dog',
      flipped: false,
      matched: false,
    },
    '1': {
      id: 1,
      image: 'cat',
      flipped: false,
      matched: false,
    },
    '2': {
      id: 2,
      image: 'dog',
      flipped: false,
      matched: false,
    },
    '3': {
      id: 3,
      image: 'lion',
      flipped: false,
      matched: false,
    },
    '4': {
      id: 4,
      image: 'lion',
      flipped: false,
      matched: false,
    },
    '5': {
      id: 5,
      image: 'tiger',
      flipped: false,
      matched: false,
    },
    '6': {
      id: 6,
      image: 'cat',
      flipped: false,
      matched: false,
    },
    '7': {
      id: 7,
      image: 'tiger',
      flipped: false,
      matched: false,
    },
  },
  cardsFlipped: 0,
  firstGuessId: null,
  isLoading: false,
  winner: false,
};

export const correctPair = [
  {
    id: 5,
    image: 'tiger',
    flipped: true,
    matched: false,
  },
  {
    id: 7,
    image: 'tiger',
    flipped: true,
    matched: false,
  },
];

export const incorrectPair = [
  {
    id: 5,
    image: 'tiger',
    flipped: true,
    matched: false,
  },
  {
    id: 6,
    image: 'cat',
    flipped: true,
    matched: false,
  },
];

export const oneCard = {
  id: 7,
  image: 'tiger',
  flipped: false,
  matched: false,
};

export const stateWithOneFlipped = {
  ...hydratedState,
  firstGuessId: 0,
  cardsById: {
    ...hydratedState.cardsById,
    0: {
      ...hydratedState.cardsById[0],
      flipped: true,
    },
  },
};
