const initialState = {
  allCards: [],
  canMakeSelection: true,
  cardsById: {},
  cardsFlipped: 0,
  firstGuessId: null,
  gameStarted: false,
  images: [
    'clyde',
    'elizabeth',
    'emmy',
    'hazel',
    'joe',
    'marcus',
    'samantha',
    'tommy',
  ],
  intervalId: null,
  isLoading: true,
  secondsElapsed: 0,
  winner: false,
};

export default initialState;
